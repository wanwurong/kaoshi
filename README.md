# 考研查分
## 一、研招网查询
[研招网](https://yz.chsi.com.cn/apply/cjcx/)


## 二、各省省教育考试院查询
### 浙江省教育考试院
[浙江省教育考试院](https://www.zjzs.net/moban/index/index.html)


### 辽宁招生考试之窗
[辽宁招生考试之窗](https://www.lnzsks.com/index.html)


### 河南省教育考试院
[河南省教育考试院](http://www.haeea.cn/)


### 湖北教育考试网
[湖北教育考试网](http://www.hbea.edu.cn/)


### 云南招生考试院
[云南招生考试院](https://www.ynzk.cn/)


### 北京教育考试院
[北京教育考试院](https://www.bjeea.cn/html/yk/)


### 天津招考资讯网
[天津招考资讯网](http://www.zhaokao.net/)


### 河北省教育考试院
[河北省教育考试院](http://www.hebeea.edu.cn/html/yzyk/index.html)


### 山西招生考试网
[山西招生考试网](http://www.sxkszx.cn/news/yjsks/index.html)


### 内蒙古招生考试信息网
[内蒙古招生考试信息网](https://www.nm.zsks.cn/)


### 吉林省教育考试院
[吉林省教育考试院](http://www.jleea.com.cn/)


### 上海招考热线
[上海招考热线](http://www.shmeea.edu.cn/page/05100/index.html)


### 江苏省教育考试院
[江苏省教育考试院](https://www.jseea.cn/webfile/)


### 青海省教育考试网
[青海省教育考试网](http://www.qhjyks.com/)


### 安徽省教育招生考试院 
[安徽省教育招生考试院](https://www.ahzsks.cn/index.htm)


### 新疆招生网 
[新疆招生网](http://www.xjzk.gov.cn/yjsks.html)


### 江西省教育考试院
[江西省教育考试院](http://www.jxeea.cn/col/col26642/index.html)


### 山东省教育招生考试院 
[山东省教育招生考试院](http://www.sdzk.cn/NewsList.aspx?BCID=4)


### 湖南省教育考试院
[湖南省教育考试院](http://jyt.hunan.gov.cn/sjyt/hnsjyksy/)


### 广东省教育考试院
[广东省教育考试院](http://eea.gd.gov.cn/yjsks/index.html)


### 广西省招生考试院 
[广西省招生考试院](https://www.gxeea.cn/yjsks/tzgg.htm)


### 海南省考试局 
[海南省考试局](http://ea.hainan.gov.cn/ywdt/ptgkyjszsb/)


### 重庆市教育考试院
[重庆市教育考试院](http://www.cqksy.cn/site/ShowClassArticleList.jsp?ClassID=287)


### 四川省教育考试院
[四川省教育考试院](https://www.sceea.cn/List/NewsList_44_1.html)


### 贵州省招生考试院
[贵州省招生考试院](http://zsksy.guizhou.gov.cn/yjsks/bmks_5375718/index.html)


### 陕西省教育考试院 
[贵州省招生考试院](http://www.sneea.cn/zcwj1/yjsks.htm)


### 甘肃省教育考试院
[甘肃省教育考试院](http://www.ganseea.cn/)


### 宁夏省教育考试院 
[宁夏省教育考试院](https://www.nxjyks.cn/contents/YJSKS/)


### 福建省教育考试院
[福建省教育考试院](http://www.eeafj.cn/ykyz/)


## 三、招生单位官网查询
### 大连理工大学研招网
[大连理工大学研招网](http://gs.dlut.edu.cn/yjszs.htm)


### 中国科学院大学招生信息网
[中国科学院大学招生信息网](https://admission.ucas.ac.cn/)


### 浙江理工大学研招网 
[浙江理工大学研招网](http://gradadmission.zstu.edu.cn/)


### 湖南大学研招网
[湖南大学研招网](http://gra.hnu.edu.cn/)


### 云南大学研招网
[云南大学研招网](http://www.grs.ynu.edu.cn/)


### 浙江大学研招网
[浙江大学研招网](http://www.grs.zju.edu.cn/yjszs/)


### 郑州大学研招网
[郑州大学研招网](http://gs.zzu.edu.cn/)


### 北京大学研招网
[北京大学研招网](https://admission.pku.edu.cn/index.htm?CSRFT=DET3-PPTD-XDU3-M7M0-6TKT-ZF3U-KOLF-KF5B)


### 清华大学研招网
[清华大学研招网](https://yz.tsinghua.edu.cn/)


### 北京师范大学研招网
